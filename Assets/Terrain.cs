﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlTypes;
using UnityEngine;

public class Terrain : MonoBehaviour
{

    public Vector3 size = new Vector3(1, 1, 1);
    public Wall wall;

    // Start is called before the first frame update
    void Start()
    {
        AddWall(new Vector3(0, 0, 0), new Vector3(size.x+2, 1, size.z+2));
        AddWall(new Vector3(0, size.y + 1, 0), new Vector3(size.x+2, 1, size.z+2));

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                AddWall(new Vector3(i * (size.x + 1), 1, j * (size.z + 1)), new Vector3(1, size.y, 1));
            }
        }
    }

    void AddWall(Vector3 p, Vector3 s)
    {
        Wall floor = GameObject.Instantiate<Wall>(wall);
        floor.transform.position = p + s/2;
        floor.transform.localScale = s;
        floor.transform.parent = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
