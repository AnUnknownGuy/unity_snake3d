﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float Speed = 0.3f;

    public bool HasEaten = false;
    public int NbRabbitsEaten = 0;

    public bool IsDead = false;


    private InputController controller;

    void Start()
    {
        controller = GetComponent<InputController>();
    }

    public void UpdateMove(float percent) {

        this.transform.position += transform.forward * percent/10 * Speed;
    }

    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "Rabbit") {
            if (!HasEaten) {
                Debug.Log("Eat !");
                HasEaten = true;
                NbRabbitsEaten = (collider.gameObject.GetComponent<Rabbit>()).Value;
            }
            
        } else if (collider.gameObject.tag == "BodyPart" && collider.gameObject.GetComponent<BodyPart>().IdBodyPart > 2) {
            IsDead = true;
        }
    }


}
