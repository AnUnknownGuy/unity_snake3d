﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rabbit : MonoBehaviour
{
    public int Value = 3;
    public bool InBody = false;

    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "BodyPart" || collider.gameObject.tag == "Player") {
            InBody = true;
        } 
    }

    public void Relocate(Vector3 location) {
        transform.position = location;
    }
}
