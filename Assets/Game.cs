﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Game : MonoBehaviour
{

    //public MethodBody body;
    public Terrain TerrainPrefab;
    public Player PlayerPrefab;
    public BodyPart BodyPartPrefab;
    public Rabbit RabbitPrefab;

    public LinkedList<Vector3> SnakeBodyPositions = new LinkedList<Vector3>();
    public Vector3 size = new Vector3(1, 1, 1);

    public float coup = 0;

    private Player Player;
    private LinkedList<BodyPart> BodyParts = new LinkedList<BodyPart>();
    
    public float timer_max = 1f;
    private float tmp_timer = 0;


    private Rabbit Rabbit;

    // Start is called before the first frame update
    void Start() {

        Terrain terrain = GameObject.Instantiate<Terrain>(TerrainPrefab);
        terrain.size = this.size;
        terrain.transform.parent = this.transform;


        Player = GameObject.Instantiate<Player>(PlayerPrefab);
        Player.transform.position = new Vector3(1.5f, 1.5f, 1.5f);
        Player.transform.parent = this.transform;


        Rabbit = GameObject.Instantiate<Rabbit>(RabbitPrefab);
        Rabbit.Relocate(new Vector3(Random.Range(1.5f, size.x), Random.Range(1.5f, size.y), Random.Range(1.5f, size.z)));
        Rabbit.transform.parent = this.transform;
        
        tmp_timer = timer_max;
    }

    // Update is called once per frame
    void Update()
    {
        if (UpdateTimer()) {
            UpdateTargets();
        }
        UpdateBodyParts();
        float percent = (timer_max - tmp_timer) / timer_max;
        Player.UpdateMove( Mathf.Sin(percent * Mathf.PI) * coup  + 1 * (1-coup));

        if (Player.HasEaten) {
            EatRabbit(Player.NbRabbitsEaten);
            Player.HasEaten = false;
            Player.NbRabbitsEaten = 0;
        }

        if (Rabbit.InBody) {
            Rabbit.Relocate(new Vector3(Random.Range(1.5f, size.x), Random.Range(1.5f, size.y), Random.Range(1.5f, size.z)));
            Rabbit.InBody = false;
        }
        
    }

    bool UpdateTimer() {
        tmp_timer -= Time.deltaTime;
        bool expired = tmp_timer <= 0;

        if (expired) {
            tmp_timer += timer_max;
        }
        return expired;
    }

    void UpdateTargets() {
        //targets.AddFirst(Player.transform);
        //targets.RemoveLast();

        //IEnumerator enumeratorBodyParts = bodyParts.GetEnumerator();
        //IEnumerator enumeratorTargets = targets.GetEnumerator();

        Transform previousTarget = Player.transform;
        foreach (BodyPart bodypart in BodyParts) {
            bodypart.SetTarget(previousTarget);
            previousTarget = bodypart.transform;
        }
    }

    public void EatRabbit(int nbParts) {
        for (int i = 0; i < nbParts; i++) {
            AddBodyPart();
        }
    }

    void AddBodyPart() {
        BodyPart newBodyPart = GameObject.Instantiate<BodyPart>(BodyPartPrefab);
        if (BodyParts.Count == 0) {
            newBodyPart.SetTarget(Player.transform);
        } else {
            newBodyPart.SetTarget(BodyParts.Last.Value.transform);
        }
        Debug.Log(BodyParts.Last);

        newBodyPart.StartPosition = newBodyPart.TargetPosition;
        BodyParts.AddLast(newBodyPart);
        newBodyPart.transform.parent = this.transform;
    }

    void UpdateBodyParts() {

        float percent = (timer_max - tmp_timer) / timer_max;
        percent = (((-Mathf.Cos(percent * Mathf.PI) + 1) / 2) * coup + percent * (1 - coup));

        foreach (BodyPart bodypart in BodyParts) {
            bodypart.UpdateMove(percent);
        }
    }
}
