﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonView : MonoBehaviour {
    public Controller MyController;
    public Player player;
    public float SmoothFactor = 0.001f;

    void Update() {

        Transform playerTransform = GetComponent<Transform>();

        player.transform.rotation = Quaternion.AngleAxis( Input.GetAxis("Vertical")* 50 * Time.deltaTime, -transform.right) * playerTransform.rotation;
        player.transform.rotation = Quaternion.AngleAxis( Input.GetAxis("Horizontal")* 50 * Time.deltaTime, transform.up) * playerTransform.rotation;
        player.transform.rotation = Quaternion.AngleAxis(Input.GetAxis("RTrigger") * 35 * Time.deltaTime, transform.forward) * playerTransform.rotation;
        player.transform.rotation = Quaternion.AngleAxis(Input.GetAxis("LTrigger") * 35 * Time.deltaTime, -transform.forward) * playerTransform.rotation;

    }
}
