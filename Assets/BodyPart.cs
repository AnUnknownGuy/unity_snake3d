﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour {

    public Vector3 StartPosition;
    public Quaternion StartRotation;

    public Vector3 TargetPosition;
    public Quaternion TargetRotation;

    public int IdBodyPart;
    public static int NbBodyParts = 0;

    void Start() {
        IdBodyPart = NbBodyParts;
        NbBodyParts += 1;
    }


    public void UpdateMove(float percent) {

        transform.position = Vector3.Lerp(StartPosition, TargetPosition, percent);
        transform.rotation = Quaternion.Lerp(StartRotation, TargetRotation, percent);
    }

    public void SetTarget(Transform target) {
        this.StartPosition = this.TargetPosition;
        this.StartRotation = this.TargetRotation;
        this.TargetPosition = target.position;
        this.TargetRotation = target.rotation;
    }
}
